#!/usr/bin/env python3
import os, sys, traceback, subprocess, shutil, argparse
import numpy as np
from scipy.misc import imread
from skimage import color


def read_image(filename, representation):
    im = imread(filename)
    if representation == 1 and im.ndim == 3 and im.shape[2] == 3:
        im = color.rgb2gray(im)
    if im.dtype == np.uint8:
        im = im.astype(np.float64) / 255.0
    if im.dtype != np.float64:
        im = im.astype(np.float64)
    return im


def presubmit(student_dir='current'):
    print('ex5 presubmission script')
    disclaimer = """
    Disclaimer
    ----------
    The purpose of this script is to make sure that your code is compliant
    with the exercise API and some of the requirements
    The script does not test the quality of your results.
    Don't assume that passing this script will guarantee that you will get
    a high grade in the exercise
    """
    print(disclaimer)

    print('=== Check Submission ===\n')
    if not os.path.exists('current/README.md'):
        print('No readme!')
        return False
    else:
        print('README file:\n')
        with open('current/README.md') as f:
            print(f.read())

    print('\n=== Answers to questions ===')
    for q in [1, 2]:
        if not os.path.exists('current/answer_q%d.txt' % q):
            print('No answer_q%d.txt!' % q)
            return False
        print('\nAnswer to Q%d:' % q)
        with open('current/answer_q%d.txt' % q) as f:
            print(f.read())
    print('\n=== Plots for Q1 ===')
    for n in ['depth_plot_denoise.png', 'depth_plot_deblur.png']:
        if not os.path.exists('current/%s' % n):
            print('Missing plot: ', n)
            return False
        print('Please verify that your plot image file %s includes the right plot.' % n)

    print('=== Load Student Library ===\n')
    print('Loading...')
    sys.stdout.flush()
    sol5 = None
    sol5_utils = None
    try:
        import current.sol5 as sol5
        import sol5_utils
    except Exception:
        print(traceback.format_exc())
        print('Unable to import the solution.')
        return False

    print('\n=== Section 3 ===\n')
    image_sets = [sol5_utils.images_for_denoising(), sol5_utils.images_for_deblurring()]
    batch_size = 100
    corruption_func = lambda x: x
    crop_size = (16, 16)
    print('Trying to load basic datasets...')
    sys.stdout.flush()
    for image_set in image_sets:
        try:
            train_set = sol5.load_dataset(image_set, batch_size, corruption_func, crop_size)
            for i in range(3):
                source, target = next(train_set)
                if source.dtype != np.float64 or target.dtype != np.float64:
                    print('source dtype: ', source.dtype)
                    raise ValueError('Wrong dtype in mini-batch!')
                if source.shape != target.shape or source.shape != (batch_size, crop_size[0], crop_size[1], 1):
                    raise ValueError('Wrong dimensions returned by source / target mini-batches!')
        except Exception:
            print(traceback.format_exc())
            return False
    print('\tPassed!')

    print('\n=== Section 4 ===\n')
    print('Trying to build a model...')
    sys.stdout.flush()
    num_channels = 32
    num_res_blocks = 3
    try:
        model = sol5.build_nn_model(crop_size[0], crop_size[1], num_channels, num_res_blocks)
        if model is None:
            raise ValueError('Returned None!')
        if model.input_shape[-3:] != (crop_size[0], crop_size[1], 1):
            raise ValueError('Wrong input shape to model!')
    except Exception:
        print(traceback.format_exc())
        return False
    print('\tPassed!')

    print('\n=== Section 5 ===\n')
    for image_set in image_sets:
        print('Trying to train a model...')
        sys.stdout.flush()
        try:
            model = sol5.build_nn_model(crop_size[0], crop_size[1], num_channels, num_res_blocks)
            train_func = lambda x: x
            batch_size = 10
            steps_per_epoch = 3
            epochs = 2
            valid_samples = 20
            sol5.train_model(model, image_set, train_func, batch_size,
                             steps_per_epoch, epochs, valid_samples)
        except Exception:
            print(traceback.format_exc())
            return False
        print('\tPassed!')

    print('\n=== Section 6 ===\n')
    print('Trying to restore image... (not checking for quality!)')
    sys.stdout.flush()
    im_orig = read_image('presubmit_externals/text.png', 1)
    try:
        im_fixed = sol5.restore_image(im_orig, model)
        if im_fixed.shape != im_orig.shape:
            raise ValueError('Returned image has a different shape than original!')
        if im_fixed.dtype != np.float64:
            raise ValueError('Returned image is not float64!')
    except Exception:
        print(traceback.format_exc())
        return False
    print('\tPassed!')

    print('\n=== Section 7 ===\n')
    print('=== Image Denoising ===')
    try:
        sys.stdout.flush()
        im_orig = read_image('presubmit_externals/image.jpg', 1)
        print('Trying to apply random noise on image...')
        sys.stdout.flush()
        im_corrupted = sol5.add_gaussian_noise(im_orig, 0, 0.2)
        if im_orig.shape != im_corrupted.shape:
            raise ValueError('Returned dimensions of noisy image do not match input!')
        if im_corrupted.dtype != np.float64:
            raise ValueError('Returned noisy image is not float64!')
        print('\tPassed!')
        print('Trying to learn a denoising model...')
        sys.stdout.flush()
        model = sol5.learn_denoising_model(num_res_blocks=num_res_blocks, quick_mode=True)
        print('\tPassed!')
        print('Trying to use learned model for denoising... (not checking for quality!)')
        sys.stdout.flush()
        im_fixed = sol5.restore_image(im_corrupted, model)
        if im_corrupted.shape != im_corrupted.shape:
            raise ValueError('Returned dimensions of fixed image do not match input!')
        if im_fixed.dtype != np.float64:
            raise ValueError('Returned fixed image is not float64!')
        print('\tPassed!')
    except Exception:
        print(traceback.format_exc())
        return False

    print('=== Image Deblurring ===')
    try:
        sys.stdout.flush()
        im_orig = read_image('presubmit_externals/text.png', 1)
        print('Trying to apply random motion blur on image...')
        sys.stdout.flush()
        im_corrupted = sol5.random_motion_blur(im_orig, [7])
        if im_orig.shape != im_corrupted.shape:
            raise ValueError('Returned dimensions of blurred image do not match input!')
        if im_corrupted.dtype != np.float64:
            raise ValueError('Returned blurred image is not float64!')
        print('\tPassed!')
        print('Trying to learn a deblurring model...')
        sys.stdout.flush()
        model = sol5.learn_deblurring_model(num_res_blocks=num_res_blocks, quick_mode=True)
        print('\tPassed!')
        print('Trying to use learned model for deblurring... (not checking for quality!)')
        sys.stdout.flush()
        im_fixed = sol5.restore_image(im_corrupted, model)
        if im_corrupted.shape != im_corrupted.shape:
            raise ValueError('Returned dimensions of fixed image do not match input!')
        if im_fixed.dtype != np.float64:
            raise ValueError('Returned fixed image is not float64!')
        print('\tPassed!')
    except Exception:
        print(traceback.format_exc())
        return False

    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'dir',
        default=None,
        nargs='?',
        help='Dummy argument for working with the CS testing system. Has no effect.'
    )
    parser.add_argument(
        '--dev',
        action='store_true',
        help='Development mode: Assumes all student files are '
             'already under the directory "./current/"'
    )
    args = parser.parse_args()
    if not args.dev:
        try:
            shutil.rmtree('current')
            shutil.rmtree('current_tmp')
        except Exception:
            pass
        os.makedirs('current_tmp')
        subprocess.check_call(['tar', 'xvf', sys.argv[1], '-C', 'current_tmp/'])
        os.rename('current_tmp/ex5', 'current')
        shutil.rmtree('current_tmp')
    if not os.path.isfile('current/__init__.py'):
        with open('current/__init__.py', 'w') as f:
            f.write(' ')
    shutil.copy('sol5_utils.py', 'current/')
    subprocess.check_call(['unzip', '-qq', 'datasets/text_dataset.zip', '-d', 'current/'])
    subprocess.check_call(['unzip', '-qq', 'datasets/image_dataset.zip', '-d', 'current/'])
    ### Supress matplotlib figures if display not available ###
    if os.getenv('DISPLAY') is None or os.getenv('DISPLAY') == '':
        import matplotlib

        matplotlib.use('PS')
    ###########
    if presubmit():
        print('\n\n=== Presubmission Completed Successfully ===')
    else:
        print('\n\n\n !!!!!!! === Presubmission Failed === !!!!!!! ')
    print("""\n
    Please go over the output and verify that there were no failures / warnings.
    Remember that this script tested only some basic technical aspects of your implementation.
    It is your responsibility to make sure your results are actually correct and not only
    technically valid.""")
