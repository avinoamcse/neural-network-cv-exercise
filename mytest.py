from imageio import imread
from skimage import color
import numpy as np
# import sol5_local
from skimage.transform import pyramid_gaussian
import matplotlib.pyplot as plt
from skimage.transform import resize

def read_image(filename, representation):
    """Reads an image, and if needed makes sure it is in [0,1] and in float64.
    arguments:
    filename -- the filename to load the image from.
    representation -- if 1 convert to grayscale. If 2 keep as RGB.
    """
    im = imread(filename)
    if representation == 1 and im.ndim == 3 and im.shape[2] == 3:
        im = color.rgb2gray(im).astype(np.float64)
    if im.dtype == np.uint8:
        im = im.astype(np.float64) / 255.0
    return im


img = read_image("C:/xKomachi/huji/image processing/ex5-avinoam/presubmit_externals/oxf.jpg",1)

def super_resolution_corruption(image):
    """
    Perform the super resolution corruption
    :param image: a grayscale image with values in the [0, 1] range of type float64.
    :return: corrupted image
    """
    from skimage.transform import resize
    factor = np.random.uniform(1, 4)
    print(factor)
    image_resized = resize(image, (np.int64(image.shape[0] // factor), (np.int64(image.shape[1] // factor))),mode='reflect',anti_aliasing=None)
    return resize(image_resized, (image.shape[0], image.shape[1]))

plt.imshow(super_resolution_corruption(img))
plt.show()