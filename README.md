# Neural Network CV Exercise

EX5 from huji's computer vision course - image manipulation with neural networks 

## Project status
This code is here for archival purposes and is not functional because the internal files on CS servers are no longer accessible. 

## File Structure
A jupiter notebook (Ex5_1_2.ipynb) is available with the last good run of the program. 
Other files are for helping with debugging/testing, as well as example images and answers to extra questions.
